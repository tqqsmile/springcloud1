package cn.tedu.sp06.filters;

import cn.tedu.web.util.JsonResult;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AccessFilter extends ZuulFilter {
    //四种过滤器的类型:pre,routing,post,error
    @Override
    public String filterType() {
        //return "pre";
        return FilterConstants.PRE_TYPE;
    }
    //过滤器的顺序号
    @Override
    public int filterOrder() {
        //有5个默认的过滤器,将自己的添加到末尾
        return 6;
    }

    //针对当前请求,判断是否执行过滤代码
    //只检查item-service 别的直接放行
    @Override
    public boolean shouldFilter() {
        //获得上下文对象
        RequestContext currentContext = RequestContext.getCurrentContext();
        String serviceId = (String) currentContext.get(FilterConstants.SERVICE_ID_KEY);//"serviceId"
        return "item-service".equals(serviceId);
    }
    //过滤代码
    @Override
    public Object run() throws ZuulException {
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
        String token = request.getParameter("token");
        if (StringUtils.isBlank(token)) {
            //阻止调用
            currentContext.setSendZuulResponse(false);
            //返回响应   jsonResult
            currentContext.addZuulRequestHeader("Content-Type"
                    , "application/json;charset=UTF-8");
            currentContext.setResponseBody(JsonResult.err().code(400)
                    .msg("请先登录!").toString());
        }
        return null;//当前zuul版本  这个返回值没有作用  随便写


    }
}
