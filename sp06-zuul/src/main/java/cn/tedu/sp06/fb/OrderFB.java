package cn.tedu.sp06.fb;
import cn.tedu.web.util.JsonResult;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Component
public class OrderFB implements FallbackProvider {
    /*
    返回一个 service-id，表示当前降级类是针对哪个后台服务的降级类，
    - item-service --- 只针对商品服务降级
    - * --- 对所有服务都应用这个降级类
    - null --- 等同于*
     */
    @Override
    public String getRoute() {
        return "order-service";
    }

    // 发回给客户端的降级响应
    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        return new ClientHttpResponse() {
            public HttpStatus getStatusCode() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR;
            }
            public int getRawStatusCode() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR.value();
            }
            public String getStatusText() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
            }
            public void close() {
            }
            public InputStream getBody() throws IOException {
                // JsonResult --- {code:500,msg:xxx,data:null}
                String json = JsonResult
                        .err().code(500).msg("调用后台服务出错").toString();
                return new ByteArrayInputStream(json.getBytes("UTF-8"));
            }
            public HttpHeaders getHeaders() {
                HttpHeaders h = new HttpHeaders();
                h.add("Content-Type", "application/json;charset=UTF-8");
                return h;
            }
        };
    }
}