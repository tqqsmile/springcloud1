package cn.tedu.sp01.service;

import cn.tedu.sp01.entity.Item;
import java.util.List;

public interface ItemService {
    //获取一个订单中的商品列表
    List<Item> getItems(String orderId);
    //减少商品库存
    void decreaseNumber(List<Item> items);
}
