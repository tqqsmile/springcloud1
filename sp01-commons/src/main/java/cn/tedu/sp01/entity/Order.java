package cn.tedu.sp01.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private String id;
    private User user;//订单所属用户
    private List<Item> items;// 购买商品列表
}
