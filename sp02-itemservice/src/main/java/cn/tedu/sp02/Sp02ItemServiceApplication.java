package cn.tedu.sp02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sp02ItemServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sp02ItemServiceApplication.class, args);
    }

}
