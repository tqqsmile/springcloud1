package cn.tedu.sp04.feign;

import cn.tedu.sp01.entity.Item;
import cn.tedu.web.util.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 三个配置:
 *  1.调用哪个服务
 *  2.调用这个服务的哪个路径
 *  3.向这个路径提交什么参数
 *
 *  可能出现的错误: ... not found : "item-service"
 *  1.服务没有启动 注册表中不存在 "item-service"
 *  2.已经启动注册服务,需要等待30秒刷新注册表
 */
@FeignClient(name = "item-service")
public interface ItemClient {

    @GetMapping("/{orderId}")
    JsonResult<List<Item>> getItems(@PathVariable String orderId);

    @PostMapping("/decreaseNumber")
    JsonResult<?> decreaseNumber(@RequestBody List<Item> items);
}
